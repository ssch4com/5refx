# Todos

* [] Formulare _registerUser_ und _newCollection_ -> Eingabefelder validieren
* [] Formular new Collection muss einen "Back-Button" erhalten
* [] Formulare _registerUser_ und _newCollection_ brauchen die Edit-Funktionalitäten
* [] Confirm beim Löschen einer Collection durch Modal ersetzen. Wirkt einfach schöner
* [x] Uhrzeiten an lastmodified und created at
* [] usercollection in die DB eintragen, wenn neu angelegt werden oder gelöscht

## Lessons learned

* Wenn eine IndexedDB-Eintrag über den _Key_ gelöscht wird, muss das als __Int__ übergeben sein!
* bei _objectSore.put(arg1, arg2)_ muss _arg2_ der _key_ (db-index) des Eintrags sein. Gilt für alle increment-felder

## Ausbau

* History / Protokoll anlegen
* logfiles für Fehlerbehandlung
* "Fehler melden" - Formular
* coachmarks für init
* Bedienungsanleitung

```
      const _itm = document.createElement('figure');
      _itm.className = "fx-refx";
      const _capt = document.createElement('figcaption');

      var _img = new Image();
      _img.src = data.filepath;
      _img.width = '240';

      _itm.appendChild(_img);


      Object.keys(data.palette).forEach((i) => {
        const _cvs = document.createElement('canvas'),
              _dim = 32;

        _cvs.width = _dim;
        _cvs.height = _dim;

        const _ctx = _cvs.getContext('2d');
        _ctx.fillStyle = 'rgb(' + data.palette[i]._rgb[0] +', ' + data.palette[i]._rgb[1] +', ' + data.palette[i]._rgb[2] +')';
        _ctx.fillRect(0, 0, _dim, _dim);
        _capt.appendChild(_cvs);

      });

      _itm.appendChild(_capt);
```