const Api = {
  VERSION: 1,
  getUser: () => {
    if (localStorage.getItem(`user-${Api.VERSION}`)) {
      return JSON.parse(localStorage.getItem(`user-${Api.VERSION}`));
    }
    else {
      return false;
    }
  },
  setUser: (user_obj) => {
    user_obj.user_id = `user-${Api.VERSION}`
    localStorage.setItem(`user-${Api.VERSION}`, JSON.stringify(user_obj));
  },
  updateUser: (user_obj) => {
    localStorage.setItem(`user-${Api.VERSION}`, JSON.stringify(user_obj));
  },
  getAllCollections: () => {
    let result = [];

    const _collArr = Api.getUser().collections;
    
    let i=0, iLen = _collArr.length;
    for (i ; i<iLen ; i++) {
      const _itm = Api.getCollection(_collArr[i]);
      result.push(_itm);
    }

    return result;
  },
  setCollection: (data, isnew) => {
    let coll_idx;
    if (isnew) {
      coll_idx = `${new Date().getTime()}-${Api.VERSION}`;
      // update user with the new collection-id
      const _usr = Api.getUser();
      _usr.collections.push(coll_idx);
      Api.updateUser(_usr);
      // extend data with user_id and coll_id
      data.user_id = _usr.user_id;
      data.coll_id = coll_idx;
    }
    else {
      coll_idx = data.coll_id;
    }
    localStorage.setItem(coll_idx, JSON.stringify(data));
  },
  getCollection: (id) => {
    return JSON.parse(localStorage.getItem(id));
  },
  deleteCollection: (id) => {
    // delete collection-id fromm users collection list
    const _usr = Api.getUser();
    const _collOnIdx = _usr.collections.indexOf(id);
    if (_collOnIdx > -1) {
      _usr.collections.splice(_collOnIdx, 1);
    }
    Api.updateUser(_usr);
    localStorage.removeItem(id);
  },
  setItem: (item) => {
    const _coll = Api.getCollection(item.coll_id);
    _coll.items.push(item);
    localStorage.setItem(item.coll_id, JSON.stringify(_coll));
  }
}