const { remote, ipcRenderer } = require('electron');
const path = require('path');


const reference = (img_path, pal_data, opts) => {
  const _itm = document.createElement('figure');
  const _capt = document.createElement('figcaption');
  
  var _img = new Image();
  _img.src = img_path;
  _img.width = '264';

  _itm.appendChild(_img);


  Object.keys(pal_data).forEach((i) => {
    const _cvs = document.createElement('canvas'),
          _dim = 44;

    _cvs.width = _dim;
    _cvs.height = _dim;

    const _ctx = _cvs.getContext('2d');
    _ctx.fillStyle = 'rgb(' + pal_data[i]._rgb[0] +', ' + pal_data[i]._rgb[1] +', ' + pal_data[i]._rgb[2] +')';
    _ctx.fillRect(0, 0, _dim, _dim);
    _capt.appendChild(_cvs);
  });

  _itm.appendChild(_capt);

  return _itm;
};


ipcRenderer.on('fileData', (event, data) => { 
  App.Collections.onAddItem(data);
});




const App = {
  Utils: {
    getObjByKeyFromArray: (arr, key, val) => {
      let i = 0, iLen = arr.length;
      for (i ; i<iLen ; i++) {
        let _itm = arr[i];
        if (_itm[key] === val) {
          return _itm;
        }
      }
      return null;
    },
    pad: (val) => {
      if (val < 10) {
        return '0' + val;
      }
      return val;
    }
  },
  Window: {
    getCurWin: () => {
      return remote.getCurrentWindow();
    },
    Actions: {
      minimize: (w = App.Window.getCurWin()) => {
        console.log('App.Window.Actions.minimize()');
        if (w.minimizable) {
          if(w.isMinimized()) {
            w.unminimize();
          }
          else {
            w.minimize();
          }
        }
      }
      , maximize: (w = App.Window.getCurWin()) => {
        console.log('App.Window.Actions.maximize()'); 
        if (w.maximizable) {
          if(w.isMaximized()) {
            w.unmaximize();
          }
          else {
            w.maximize();
          }
        }
      }
      , close: (w = App.Window.getCurWin()) => {
        console.log('App.Window.Actions.close()');
        w.close();
      }
    }
  },
  Inits: {
    start: () => {

      // check if user is existing
      const _hasUser = Api.getUser();
      if (!_hasUser) {
        App.Draw.registerUser();
      }
      else {
        App.Draw.dashboard();
      }

      // initialize floating button
      const _collBtn = document.querySelectorAll('.fixed-action-btn');
      const inst_collBtn = M.FloatingActionButton.init(_collBtn, {});
    }
  },
  Draw: {
    clean: () => {
      const contents = document.getElementById('FX-MainContent');
      contents.innerHTML = '';
    },
    Header: {
      init: (user) => {
        const _target = document.getElementById('FX-UserName');
        _target.innerHTML = user.name;
        _target.removeAttribute('hidden');
      }
    },
    newCollection: (collection) => {
      App.Draw.clean();

      let collId = ``, collName = ``, collClr = '#757575', isNew = 'TRUE'; 

      if (typeof collection !== 'undefined') {
        // Hier laufen dann die bekannten Daten rein
        collName = collection.name;
        collClr = collection.color;
        isNew = 'FALSE'
        collId = `<input type="hidden" id="coll_id" name="coll_id" value="${collection.coll_id}">`;
      }
      const tpl = `
        <div class="row">
          <form class="col s6 offset-s3" onsubmit="App.Collections.store(this, event); return false;">
            <input type="hidden" id="coll_isnew" name="coll_isnew" value="${isNew}">
            ${collId}
            <div class="input-field">
              <input id="coll_name" type="text" class="validate" ${ (collName != '') ? 'value="' + collName + '"': ''}>
              <label for="coll_name">Name</label>
            </div>
            <div class="fx-colorChoose">
              <label class="fx-colorWrap">
                <input class="fx-radio" name="coll_color" value="757575" type="radio" ${ (collClr === '#757575') ? 'checked' : '' } />
                <div class="fx-label">
                  <div style="background-color: #757575;" class="inactive"></div>
                  <div style="background-color: #757575;" class="active"></div>
                </div>
              </label>
              <label class="fx-colorWrap">
                <input class="fx-radio" name="coll_color" value="f44336" type="radio" ${ (collClr === '#f44336') ? 'checked' : '' } />
                <div class="fx-label">
                  <div style="background-color: #f44336;" class="inactive"></div>
                  <div style="background-color: #f44336;" class="active"></div>
                </div>
              </label>
              <label class="fx-colorWrap">
                <input class="fx-radio" name="coll_color" value="ffc107" type="radio" ${ (collClr === '#ffc107') ? 'checked' : '' } />
                <div class="fx-label">
                  <div style="background-color: #ffc107;" class="inactive"></div>
                  <div style="background-color: #ffc107;" class="active"></div>
                </div>
              </label>
              <label class="fx-colorWrap">
                <input class="fx-radio" name="coll_color" value="9c27b0" type="radio" ${ (collClr === '#9c27b0') ? 'checked' : '' } />
                <div class="fx-label">
                  <div style="background-color: #9c27b0;" class="inactive"></div>
                  <div style="background-color: #9c27b0;" class="active"></div>
                </div>
              </label>
              <label class="fx-colorWrap">
                <input class="fx-radio" name="coll_color" value="2196f3" type="radio" ${ (collClr === '#2196f3') ? 'checked' : '' } />
                <div class="fx-label">
                  <div style="background-color: #2196f3;" class="inactive"></div>
                  <div style="background-color: #2196f3;" class="active"></div>
                </div>
              </label>
              <label class="fx-colorWrap">
                <input class="fx-radio" name="coll_color" value="009688" type="radio" ${ (collClr === '#009688') ? 'checked' : '' } />
                <div class="fx-label">
                  <div style="background-color: #009688;" class="inactive"></div>
                  <div style="background-color: #009688;" class="active"></div>
                </div>
              </label>
            </div>
            <button onclick="App.Draw.dashboard(); return false;" class="waves-effect waves-light btn blue-grey">
              <i class="icon-undo left"></i>
            </button>
            <button type="submit" class="waves-effect waves-light btn indigo lighten-1 right">
              ${ (isNew === 'TRUE') ? 'Add New' : 'Update' }
            </button>
          </form>
        </div>
      `;

      document.getElementById('FX-MainContent').innerHTML = tpl;
      document.getElementById('coll_name').focus(); 
    },
    registerUser: (data) => {
      App.Draw.clean();
      
      const tpl = `
        <div class="row">
          <form class="col s6 offset-s3" onsubmit="App.Register.new(this, event); return false;">
            <div class="input-field">
              <input id="name" type="text" class="validate">
              <label for="name">Name</label>
            </div>
            <div class="input-field">
              <input id="email" type="text" class="validate">
              <label for="email">E-Mail</label>
            </div>
            <div class="input-field">
              <input id="company" type="text" class="validate">
              <label for="company">Company</label>
            </div>
            <button type="submit" class="waves-effect waves-light btn indigo lighten-1 right">
              <i class="icon-send right"></i>register
            </button>
          </form>
        </div>
      `;
      document.getElementById('FX-MainContent').innerHTML = tpl;
      document.getElementById('name').focus();
    },
    dashboard: (data) => {
      App.Draw.clean();
      const _allColl = Api.getAllCollections();

      let i = 0, iLen = _allColl.length, li_str = ``;
      if (iLen > 0) {
        for (i ; i<iLen ; i++) {
          var _itm = _allColl[i];
          const _created = `${new Date(_itm.createdAt).toLocaleDateString()} ${App.Utils.pad(new Date(_itm.createdAt).getHours())}:${App.Utils.pad(new Date(_itm.createdAt).getMinutes())}`;
          const _modified = `${new Date(_itm.lastModified).toLocaleDateString()} ${App.Utils.pad(new Date(_itm.lastModified).getHours())}:${App.Utils.pad(new Date(_itm.lastModified).getMinutes())}`;

          li_str += `
            <section style="border-color: ${_itm.color};" class="fx-collItem" onclick="App.Collections.view(this, event, { collId: '${_itm.coll_id}', userId: '${_itm.user_id}' }); return false;">
              <header class="fx-collHeader">
                <h4>${_itm.name}</h4>
              </header>

              <div class="fx-collInfo">
                <div class="fx-collInfoWrap">
                  <time class="fx-collCreated">created: ${_created}</time>
                  <time class="fx-collModified">updated: ${_modified}</time>
                </div>
                <div class="fx-collAction">
                  <button 
                      style="color: ${_itm.color}; border-color: ${_itm.color};"
                      onclick="App.Collections.view(this, event, { collId: '${_itm.coll_id}', userId: '${_itm.user_id}' })" 
                      title="view">
                      <i class="icon-remove_red_eye"></i>
                  </button>
                  <button 
                      style="color: ${_itm.color}; border-color: ${_itm.color};"
                      onclick="App.Collections.edit(this, event, { collId: '${_itm.coll_id}', userId: '${_itm.user_id}' })" 
                      title="edit">
                      <i class="icon-create"></i>
                  </button>
                  <button 
                      style="color: ${_itm.color}; border-color: ${_itm.color};"
                      onclick="App.Collections.delete(this, event, { collId: '${_itm.coll_id}', userId: '${_itm.user_id}' })" 
                      title="delete">
                      <i class="icon-delete"></i>
                  </button>
                </div>
                <div style="background-color: ${_itm.color};" class="fx-collItems">
                  <span>${_itm.items.length}</span>
                </div>
              </div>
            </section>
          `;
        }
      }
      else {
        li_str += `<p>No Collections yet</p>`;
      }

      const tpl = `
        <div class="fx-collection">
          ${li_str}
        </div>
      `;
      document.getElementById('FX-MainContent').innerHTML = tpl;
    },
    getTplStringForCollItem: (data) => {
      if (data.length <= 0) {
        return `<p>No Refereces</p>`;
      }

      let i=0, iLen=data.length, tplStr=``;
      for( i ; i<iLen ; i++) {
        const _ref = data[i];

        let clrStr=``;
        Object.keys(_ref.palette).forEach((p) => {
          let _color = `rgb(${_ref.palette[p]._rgb[0]}, ${_ref.palette[p]._rgb[1]}, ${_ref.palette[p]._rgb[2]})`;
          clrStr += `<a draggable="true" href="#" onclick="return false;" style="display: inline-block; width: 32px; height: 32px; background: ${_color};"></a>`;
        });

        tplStr += `
          <figure class="fx-refx">
            <img title="${_ref.filepath}" src="${_ref.filepath}" width="240" />
            <figcaption>
              <ul>
                ${clrStr}
              </ul>
            </figcaption>
          </figure>
        `;
      }

      return tplStr; 
    },
    collectionCanvas: (data) => {
      App.Draw.clean();

      const coll = Api.getCollection(data.collId);

      // format times
      const _created = `${new Date(coll.createdAt).toLocaleDateString()} ${App.Utils.pad(new Date(coll.createdAt).getHours())}:${App.Utils.pad(new Date(coll.createdAt).getMinutes())}`;
      const _modified = `${new Date(coll.lastModified).toLocaleDateString()} ${App.Utils.pad(new Date(coll.lastModified).getHours())}:${App.Utils.pad(new Date(coll.lastModified).getMinutes())}`;

      // get item templatestrings
      const tpl_items = App.Draw.getTplStringForCollItem(coll.items);

      const tpl = `
        <section class="fx-collItemsWrap">
          <header style="color: ${coll.color};">
            <h4>
              <button 
                  class="fx-back"
                  style="color: ${coll.color}; border-color: ${coll.color};"
                  onclick="App.Draw.dashboard();" 
                  title="view">
                  <i class="icon-undo"></i>
              </button>
              <span>${coll.name}</span>
              <small>${_created} | ${_modified}</small>
            </h4>
          </header>
          <div>
            <div class="fx-collItems" id="FX-CollItems">
              ${tpl_items}
            </div>
          </div>
        </section>
      `;

      document.getElementById('FX-MainContent').innerHTML = tpl;

      // register events
      const _drop = document.getElementById('FX-MainContent');
      _drop.ondrag = () => {
        console.log('--- ondrag')
        return false;
      };
      _drop.ondragover = () => {
        console.log('--- ondragover')
        return false;
      };
      _drop.ondragleave = () => {
        console.log('--- ondragleave')
        return false;
      };
      _drop.ondragend = () => {
        console.log('--- ondragend')
        return false;
      };

      _drop.ondrop = (e) => {
        e.preventDefault();
        console.log('--- ondrop')

        for (let f of e.dataTransfer.files) {
          console.dir(f);
          console.log('---------------');

          App.Collections.addItem(f.path, data.collId);
        }
        return false;
      }
    }
  },
  Register: {
    'new': (form, ev) => {
      ev.preventDefault();

      const user_data = {
        name: form.elements['name'].value
        , email: form.elements['email'].value
        , company: form.elements['company'].value
        , collections: []
      };

      Api.setUser(user_data);
      App.Draw.dashboard();
      App.Draw.Header.init(user_data);
    }
  },
  Collections: {
    store: (form, ev) => {
      ev.preventDefault();
      let coll_data, isNew = false;
      if (form.elements['coll_isnew'].value === 'TRUE') {
        isNew = true;
        coll_data = {
            name: form.elements['coll_name'].value
          , color: '#' + form.elements['coll_color'].value
          , lastModified: new Date().getTime()
          , createdAt: new Date().getTime()
          , items: []
        };
      }
      else {
        coll_data = Api.getCollection(form.elements['coll_id'].value);
        coll_data.name = form.elements['coll_name'].value;
        coll_data.color = '#' + form.elements['coll_color'].value
        coll_data.lastModified = new Date().getTime();
      }
      Api.setCollection(coll_data, isNew);
      App.Draw.dashboard();
    },
    view: (el, ev, data) => {
      console.log(el, ev, data);
      ev.stopPropagation();
      App.Draw.collectionCanvas(data);
    },
    edit: (el, ev, data) => {
      ev.stopPropagation();
      const _collData = Api.getCollection(data.collId);
      App.Draw.newCollection(_collData);
    },
    delete: (el, ev, data) => {
      ev.stopPropagation();

      Api.deleteCollection(data.collId);
      App.Draw.dashboard();
    },
    addItem: (path, coll_id) => {
      const q = {
        path: path,
        coll_id: coll_id
      }
      ipcRenderer.send('ondragstart', q);
    },
    onAddItem: (data) => {
      const _refContainer = document.getElementById('FX-CollItems');
      Api.setItem(data)
      App.Draw.collectionCanvas({collId: data.coll_id});
    }
  }
};

document.addEventListener('DOMContentLoaded', function() {
  App.Inits.start();
});