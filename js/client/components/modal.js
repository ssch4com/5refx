const template = document.createElement('template');
template.innerHTML = `
  <div class="modal-content">
    <header>
      <h4>STATIC TITLE</h4>
      <small>optional subtitle</small>
    </header>
    <div>
      MODALCONTENT
    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" onclick="App.Collections.store();" class="modal-close waves-effect waves-green btn-flat">Ok</a>
    <a href="#!" onclick="App.Collections.Modals.close();" class="modal-close waves-effect waves-green btn-flat">Cancel</a>
  </div>
`;


class FxModal extends HTMLElement {
  constructor() {
    console.log('Constructor called for FxModal')
    super();
    // element created
    const templ = document.getElementById('Fx-Modal');

    this._shadowRoot = this.attachShadow( {mode: 'open'} );
    this._shadowRoot.appendChild(template.content.cloneNode(true));

    this.$title = this._shadowRoot.querySelector('header h4');
    this.$subtitle = this._shadowRoot.querySelector('header small');
  }

  get title() {
    return this.getAttribute('title');
  }
  set title(val) {
    this.setAttribute('title', val);
  }

  get subtitle() {
    return this.getAttribute('subtitle');
  }
  set subtitle(val) {
    this.setAttribute('subtitle', val);
  }

  connectedCallback() {
    console.log('Browser calls method - connectedCallback')
    // browser calls this method when the element is added to the document
    // (can be called many times if an element is repeatedly added/removed)
  }

  disconnectedCallback() {
    console.log('Browser calls method - disconnectedCallback')
    // browser calls this method when the element is removed from the document
    // (can be called many times if an element is repeatedly added/removed)
  }

  static get observedAttributes() {
    return ['title'];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    this.render();
  }

  adoptedCallback() {
    // called when the element is moved to a new document
    // (happens in document.adoptNode, very rarely used)
  }

  render() {
    this.$title.innerHTML = this.title;
    this.$subtitle.innerHTML = this.subtitle;
  }

  // there can be other element methods and properties
}

customElements.define("fx-modal", FxModal);