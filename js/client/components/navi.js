class FxNavi extends HTMLElement {
  constructor() {
    console.log('Constructor called for FxNavi')
    super();

    const template = document.createElement('template');
    template.innerHTML = `
      <style>
        .fx-pageNav {

        }
        .fx-pageNav header h2 {
          font-size: 80%;
          text-transform: uppercase;
        }
      </style>

      <nav class="fx-pageNav">
        <header>
          <h2>Your Collections</h2>
        </header>
        <ul id="FX-NavList" class="fx-navList">
          <li>No Collections</li>
        </ul>
      </nav>
    `;

    this._shadowRoot = this.attachShadow( {mode: 'open'} );
    this._shadowRoot.appendChild(template.content.cloneNode(true));
  }

  connectedCallback() {
    console.log('Browser calls method - connectedCallback')
    // browser calls this method when the element is added to the document
    // (can be called many times if an element is repeatedly added/removed)
  }

  disconnectedCallback() {
    console.log('Browser calls method - disconnectedCallback')
    // browser calls this method when the element is removed from the document
    // (can be called many times if an element is repeatedly added/removed)
  }

  static get observedAttributes() {

  }

  attributeChangedCallback(name, oldValue, newValue) {
    this.render();
  }

  adoptedCallback() {
    // called when the element is moved to a new document
    // (happens in document.adoptNode, very rarely used)
  }

  render() {

  }

  // there can be other element methods and properties
}

customElements.define("fx-nav", FxNavi);