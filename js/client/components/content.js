class FxContent extends HTMLElement {
  constructor() {
    console.log('Constructor called for FxContent')
    super();

    const template = document.createElement('template');
    template.innerHTML = this.getTemplate(this.id);

    // element created
    this._shadowRoot = this.attachShadow( {mode: 'open'} );
    this._shadowRoot.appendChild(template.content.cloneNode(true));
  }

  getTemplate(el_id) {

    const tpl = {
      'FX-Loader': `
        <div>Getting Content ...</div>
      `,
      'FX-NewCollection': `
        <style>
          .fx-createCollection header h2 {
            font-size: 80%;
            text-transform: uppercase;
          }
          .fx-createCollection .fx-action {
            margin-top: 1em;
          }
          .fx-createCollection .fx-action .fx-submit {
            color: rgba(255,255,255,0.4);
            border: 1px solid rgba(255,255,255,0.4);
            background: none;
            font-size: 100%;
            text-transform: uppercase;
            border-radius: 3px;
            padding: 4px 8px;
          }
        </style>
        <section class="fx-createCollection">
          <header>
            <h2>Create new collection</h2>
          </header>
          <form onsubmit="App.Collections.new(this, event); return false;">
            <fieldset>
              <legend>New collection</legend>
              <div class="fx-controlGroup">
                <label for="coll_name">Name</label>
                <div class="fx-formControl">
                  <input id="coll_name" type="text" />
                </div>
              </div>
              <div class="fx-action">
                <button type="submit" class="fx-submit">Create</button>
              </div>
            </fieldset>
          </form>
        </section>
      `,
      'FX-Dashboard': `
        <style>
          .fx-loader header h2 {
            font-size: 80%;
            text-transform: uppercase;
          }
        </style>
        <section class="fx-loader">
          <header>
            <h2>Dashboard</h2>
          </header>
        </section>
      `,
      'FX-RegisterUser': `
        <style>
          .fx-form header h2 {
            font-size: 80%;
            text-transform: uppercase;
          }
          .fx-form .fx-action {
            margin-top: 1em;
          }
          .fx-form .fx-action .fx-send {
            color: rgba(255,255,255,0.4);
            border: 1px solid rgba(255,255,255,0.4);
            background: none;
            font-size: 100%;
            text-transform: uppercase;
            border-radius: 3px;
            padding: 4px 8px;
          }

        </style>
        <section class="fx-form fx-registerUser">
          <header>
            <h2>Welcome, please register</h2>
          </header>
          <div class="fx-controlGroup">
            <label for="register_name">Name</label>
            <div class="fx-formControl">
              <input id="register_name" type="text" />
            </div>
          </div>
          <div class="fx-controlGroup">
            <label for="register_email">E-Mail</label>
            <div class="fx-formControl">
              <input id="register_email" type="email" />
            </div>
          </div>
          <div class="fx-controlGroup">
            <label for="register_company">Company</label>
            <div class="fx-formControl">
              <input id="register_company" type="text" />
            </div>
          </div>
          <div class="fx-action">
            <button onclick="App.Register.new(['register_name', 'register_email', 'register_company']); return false;" class="fx-send">Register</button>
          </div>
        </section>
      `
    }


    return tpl[el_id];
  }

  connectedCallback() {
    console.log('Browser calls method - connectedCallback')
    // browser calls this method when the element is added to the document
    // (can be called many times if an element is repeatedly added/removed)
  }

  disconnectedCallback() {
    console.log('Browser calls method - disconnectedCallback')
    // browser calls this method when the element is removed from the document
    // (can be called many times if an element is repeatedly added/removed)
  }

  static get observedAttributes() {

  }

  attributeChangedCallback(name, oldValue, newValue) {
    this.render();
  }

  adoptedCallback() {
    // called when the element is moved to a new document
    // (happens in document.adoptNode, very rarely used)
  }

  render() {

  }

  // there can be other element methods and properties
}

customElements.define("fx-content", FxContent);