const { app, BrowserWindow, ipcMain } = require('electron');
const { dialog } = require('electron');
const url = require('url');
const fs = require('fs');
const path = require('path');
const Vibrant = require('node-vibrant');

function createWindow()  {
let win = new BrowserWindow({
      width: 1180
    , height: 768
    , titleBarStyle: 'hidden'
    , frame: false
    , darkTheme: true
    , webPreferences: {
      nodeIntegration: true
      , nativeWindowOpen: true
      , enableRemoteModule: true
    }
  });
  win.loadURL(url.format({
    pathname: path.join(__dirname, '/index.html')
    , protocol: 'file:'
    , slashes: true
  }));

  win.openDevTools();
}

app
  .whenReady()
  .then(createWindow);

app
  .on('window-all-closed', () => {

    if (process.platform !== 'darwin') {
      app.quit();
    }
  })
  .on('activate', () => {

    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow();
    }
  });


ipcMain.on('ondragstart', (event, data) => {
  Vibrant
    .from(data.path, {colorCount: 128})
    .quality(1)
    .getPalette((err, palette) => {
    var res = {
      filepath: data.path
      , palette: palette
      , coll_id: data.coll_id
    }
    event.sender.send('fileData', res);
  });
});